#! /usr/bin/env node

var cloudsheep = require('../lib/cloudsheep');
var args = process.argv.slice(2);

var action = args.shift();

if (!action) {
    action = 'create';
}

if (!cloudsheep[action]) {
    console.error('Unkown action: ' + action);
    process.exit();
}

cloudsheep[action](args);
