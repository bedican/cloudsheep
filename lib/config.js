function Config(data) {
    this.data = data;
    this.json = JSON.parse(data);
}

Config.prototype.getData = function() {
    return this.data;
};

Config.prototype.getDescription = function() {
    return this.json['Description'];
};

Config.prototype.getParameterNames = function() {

    var names = [];

    for(var key in this.json['Parameters']) {
        names.push(key);
    }

    return names;
};

Config.prototype.getParameter = function(name) {
    return this.json['Parameters'][name] || {};
};

module.exports = {
    new: function (data) {
        return new Config(data);
    }
};
