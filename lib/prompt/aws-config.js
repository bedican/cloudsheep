var prompt = require('prompt');

var options = {
    accessKeyId: "Access key id",
    secretAccessKey: "Secret access key",
    region: "Region"
};

module.exports = {
    prompt: function(parameters, defaults, callback) {

        var property, propertyName;
        var promptProperties = {};

        for(var key in parameters) {

            parameterName = parameters[key];

            if (!options[parameterName]) {
                continue;
            }

            property = {
                required: true,
                description: options[parameterName].cyan
            };

            if (defaults[parameterName]) {
                property.default = defaults[parameterName];
            }

            promptProperties[parameterName] = property;
        }

        prompt.message = "cloudsheep > ".bold.green;
        prompt.delimiter = "";

        prompt.start();

        prompt.get({ properties: promptProperties }, function (err, result) {
            return callback(result);
        });
    }
};
