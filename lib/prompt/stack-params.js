var prompt = require('prompt');

module.exports = {
    prompt: function(config, defaults, callback) {

        defaults = defaults || {};

        var parameterName, parameterValue, property;
        var parameterNames = config.getParameterNames();

        var promptProperties = {};

        for(var key in parameterNames) {

            parameterName = parameterNames[key];
            parameterValue = config.getParameter(parameterName);

            property = {
                required: true,
                description: (parameterValue.Description || parameterName).cyan
            };

            if (defaults[parameterName]) {
                property.default = defaults[parameterName];
            }
            else if(parameterValue.Default) {
                property.default = parameterValue.Default;
            }
            if(parameterValue.AllowedValues) {
                property.enum = parameterValue.AllowedValues;
            }
            if(parameterValue.AllowedPattern) {
                property.pattern = parameterValue.AllowedPattern;
            }
            if(parameterValue.ConstraintDescription) {
                property.message = parameterValue.ConstraintDescription;
            }

            promptProperties[parameterName] = property;
        }

        prompt.message = "cloudsheep > ".bold.green;
        prompt.delimiter = "";

        prompt.start();

        prompt.get({ properties: promptProperties }, function (err, result) {
            return callback(result);
        });
    }
};
