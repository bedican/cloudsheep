var prompt = require('prompt');

module.exports = {
    prompt: function(callback) {

        prompt.message = "cloudsheep > ".bold.green;
        prompt.delimiter = "";

        prompt.start();

        var properties = {
            required: true,
            name: 'yesno',
            message: 'are you sure [yN]?'.cyan,
            validator: /[yn]/i,
            warning: 'Please select y or n',
            default: 'n',
            before: function(value) { return value.toLowerCase(); }
        };

        prompt.get(properties, function (err, result) {
            callback(result.yesno === 'y');
        });
    }
};
