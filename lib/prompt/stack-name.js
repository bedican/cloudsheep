var prompt = require('prompt');

module.exports = {
    prompt: function(callback) {

        var promptProperties = {
            StackName: {
                required: true,
                description: "Stack name".cyan
            }
        };

        prompt.message = "cloudsheep > ".bold.green;
        prompt.delimiter = "";

        prompt.start();

        prompt.get({ properties: promptProperties }, function (err, result) {
            return callback(result.StackName);
        });
    }
};
