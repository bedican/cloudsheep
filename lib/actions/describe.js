var AWS = require('aws-sdk');
var Table = require('cli-table');
var configure = require('./configure');

module.exports = {
    run: function(args) {

        var _this = this;
        var stackName = args[0];

        configure.configure(function() {
            _this.describe(stackName, function(data) {

                var consoleWidth = process.stdout.getWindowSize()[0];

                var table = new Table({
                    head: ['Name'.bold.cyan, 'Value'.bold.cyan],
                    colWidths: [
                        parseInt((consoleWidth / 100) * 20),
                        parseInt((consoleWidth / 100) * 76)
                    ],
                    truncate: false
                });

                for (var key in data) {
                    table.push(
                        [key, (typeof data[key] == "object") ? JSON.stringify(data[key], null, 4) : data[key]]
                    );
                }

                console.info(table.toString());
            });
        });
    },
    describe: function(stackName, callback) {

        if (!stackName) {
            console.error('Missing stackName'.red);
            return;
        }

        configure.configure(function() {
            var cf = new AWS.CloudFormation({apiVersion: '2010-05-15'});

            var params = {
                StackName: stackName
            };

            cf.describeStacks(params, function(err, data) {
                if (err) {
                    console.error(err.message.red);
                    return;
                }

                callback(data.Stacks[0]);
            });
        });
    }
};
