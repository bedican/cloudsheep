var AWS = require('aws-sdk');
var configure = require('./configure');
var describe = require('./describe');
var events = require('./events');
var loader = require('../loader');
var promptStackName = require('../prompt/stack-name');
var promptStackParams = require('../prompt/stack-params');

module.exports = {
    run: function(args) {

        // TODO: if no filename supplied, the loader should obtain
        // the existing template from aws, with getTemplate
        var filename = args[0] || 'cf.json';

        configure.configure(function() {
            loader.config(filename, function(config) {

                console.info(config.getDescription().bold.yellow + "\n");

                promptStackName.prompt(function (stackName) {
                    describe.describe(stackName, function (stack) {

                        var defaults = {};
                        for(var key in stack.Parameters) {
                            defaults[stack.Parameters[key].ParameterKey] = stack.Parameters[key].ParameterValue;
                        }

                        promptStackParams.prompt(config, defaults, function (parameters) {

                            var cf = new AWS.CloudFormation({apiVersion: '2010-05-15'});
                            var stackParameters = [];

                            for(var key in parameters) {
                                stackParameters.push({
                                    ParameterKey: key,
                                    ParameterValue: parameters[key]
                                });
                            }

                            var params = {
                                StackName: stackName,
                                Capabilities: [ 'CAPABILITY_IAM' ],
                                Parameters: stackParameters,
                                TemplateBody: config.getData()
                            };

                            cf.updateStack(params, function(err, data) {
                                if (err) {
                                    console.error(err.message.red);
                                    return;
                                }

                                console.info(('Updating stack ID: ' + data.StackId).green);

                                events.events(data.StackId);
                            });
                        });
                    });
                });
            });
        });
    }
};
