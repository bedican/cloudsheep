var AWS = require('aws-sdk');
var Table = require('cli-table');
var configure = require('./configure');

module.exports = {
    run: function() {
        configure.configure(function () {
            var cf = new AWS.CloudFormation({apiVersion: '2010-05-15'});

            var params = {
                StackStatusFilter: [ 'CREATE_COMPLETE' ]
            };

            cf.listStacks(params, function(err, data) {
                if (err) {
                    console.error(err.message.red);
                    return;
                }

                var consoleWidth = process.stdout.getWindowSize()[0];

                var table = new Table({
                    head: ['Stack Name'.bold.cyan, 'Stack Id'.bold.cyan],
                    colWidths: [
                        parseInt((consoleWidth / 100) * 20),
                        parseInt((consoleWidth / 100) * 76)
                    ],
                    truncate: false
                });

                var stacks = data.StackSummaries;

                for(var key in stacks) {
                    table.push(
                        [stacks[key].StackName, stacks[key].StackId]
                    );
                }

                console.info(table.toString());
            });
        });
    }
};
