var AWS = require('aws-sdk');
var configure = require('./configure');
var loader = require('../loader');

module.exports = {
    run: function(args) {
        var filename = args[0] || 'cf.json';

        configure.configure(function () {
            loader.load(filename, function(data) {

                var cf = new AWS.CloudFormation({apiVersion: '2010-05-15'});

                var params = {
                    TemplateBody: data
                };

                cf.validateTemplate(params, function(err, data) {
                    if (err) {
                        console.error(err.message.red);
                        return;
                    }

                    console.info('Valid'.green);
                });
            });
        });
    }
};
