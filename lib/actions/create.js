var AWS = require('aws-sdk');
var configure = require('./configure');
var events = require('./events');
var loader = require('../loader');
var promptStackName = require('../prompt/stack-name');
var promptStackParams = require('../prompt/stack-params');

module.exports = {
    run: function(args) {
        var _this = this;
        var filename = args[0] || 'cf.json';

        configure.configure(function () {
            loader.config(filename, function(config) {

                console.info(config.getDescription().bold.yellow + "\n");

                promptStackName.prompt(function (stackName) {
                    promptStackParams.prompt(config, {}, function (parameters) {

                        var cf = new AWS.CloudFormation({apiVersion: '2010-05-15'});
                        var stackParameters = [];

                        for(var key in parameters) {
                            stackParameters.push({
                                ParameterKey: key,
                                ParameterValue: parameters[key]
                            });
                        }

                        var params = {
                            StackName: stackName,
                            Capabilities: [ 'CAPABILITY_IAM' ],
                            OnFailure: 'ROLLBACK',
                            Parameters: stackParameters,
                            TemplateBody: config.getData(),
                            TimeoutInMinutes: 1
                        };

                        cf.createStack(params, function(err, data) {
                            if (err) {
                                console.error(err.message.red);
                                return;
                            }

                            console.info(('Creating stack ID: ' + data.StackId).green);

                            events.events(data.StackId);
                        });
                    });
                });
            });
        });
    }
};
