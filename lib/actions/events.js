var AWS = require('aws-sdk');
var Table = require('cli-table');
var sleep = require('sleep');
var configure = require('./configure');

var lastEventId = null;

function describeStackEvents(cf, params, callback) {
    cf.describeStackEvents(params, function(err, data) {
        if (err) {
            console.error(err.message.red);
            return;
        }

        var events = [];
        var stackEvents = data.StackEvents;

        if (lastEventId) {
            for(var key in stackEvents) {
                if (stackEvents[key].EventId == lastEventId) {
                    break;
                }

                events.push(stackEvents[key]);
            }
        } else {
            events = stackEvents;
        }

        if (events.length) {
            lastEventId = events[0].EventId;
        }

        callback(events.reverse());

        if (
            (stackEvents.length) &&
            (stackEvents[0].ResourceType === 'AWS::CloudFormation::Stack') &&
            ([
                'CREATE_COMPLETE',
                'CREATE_FAILED',
                'DELETE_COMPLETE',
                'DELETE_FAILED',
                'UPDATE_COMPLETE',
                'UPDATE_FAILED',
                'ROLLBACK_COMPLETE'
            ].indexOf(stackEvents[0].ResourceStatus) !== -1)
        ) {
            return;
        }

        sleep.sleep(3);

        if (data.NextToken) {
            params.NextToken = data.NextToken;
        }

        describeStackEvents(cf, params, callback);
    });
}

module.exports = {
    run: function(args) {
        var _this = this;
        var stackName = args[0];

        configure.configure(function() {
            _this.events(stackName);
        });
    },
    events: function(stackName) {
        if (!stackName) {
            console.error('Missing stackName'.red);
            return;
        }

        var cf = new AWS.CloudFormation({apiVersion: '2010-05-15'});

        var params = {
            StackName: stackName
        };

        var consoleWidth = process.stdout.getWindowSize()[0];
        var colWidths = [
            parseInt((consoleWidth / 100) * 8),
            parseInt((consoleWidth / 100) * 30),
            parseInt((consoleWidth / 100) * 20),
            parseInt((consoleWidth / 100) * 38)
        ];
        var tableChars = {
            'top': '' , 'top-mid': '' , 'top-left': '' , 'top-right': '',
            'bottom': '' , 'bottom-mid': '' , 'bottom-left': '' , 'bottom-right': '',
            'left': '' , 'left-mid': '' , 'mid': '' , 'mid-mid': '',
            'right': '' , 'right-mid': '' , 'middle': ' '
        };

        var table = new Table({
            head: ['Stack'.bold.cyan, 'Resource Id'.bold.cyan, 'Resource status'.bold.cyan, 'Resource status reason'.bold.cyan],
            colWidths: colWidths,
            chars: tableChars
        });

        console.info();
        console.info(table.toString());
        console.info();

        describeStackEvents(cf, params, function(events) {

            if (!events.length) {
                return;
            }

            var table = new Table({
                colWidths: colWidths,
                truncate: false,
                chars: tableChars
            });

            for(var key in events) {
                table.push([
                    events[key].StackName,
                    events[key].LogicalResourceId,
                    events[key].ResourceStatus,
                    events[key].ResourceStatusReason || ''
                ]);
            }

            console.info(table.toString());
        });
    }
};
