var AWS = require('aws-sdk');
var fs = require('fs');
var prompt = require('../prompt/aws-config');

var configure = {

    filename: process.cwd() + '/.cloudsheep.aws.json',
    options: [
        'accessKeyId',
        'secretAccessKey',
        'region'
    ],

    load: function() {
        AWS.config.loadFromPath(this.filename);
    },
    read: function(callback) {
        var _this = this;

        fs.exists(this.filename, function (exists) {
            if (!exists) {
                callback({});
                return;
            }

            fs.readFile(_this.filename, 'utf8', function (err, data) {
                if(err) throw err;
                callback(JSON.parse(data));
            });
        });
    },
    write: function(json, callback) {
        fs.writeFile(this.filename, JSON.stringify(json), function(err) {
            if(err) throw err;
            if(callback) callback();
        });
    },
    promptMissing: function(callback) {

        var _this = this;

        this.read(function(json) {

            var missing = [];

            for(var key in _this.options) {
                if (!json[_this.options[key]]) {
                    missing.push(_this.options[key]);
                }
            }

            if (!missing.length) {
                callback();
                return;
            }

            console.info("Please provide missing aws configuration".bold.yellow + "\n");

            prompt.prompt(missing, {}, function (parameters) {
                for (var key in parameters) {
                    json[key] = parameters[key];
                }

                console.info();

                _this.write(json, callback);
            });
        });
    },
    promptAll: function() {

        var _this = this;

        this.read(function(json) {
            prompt.prompt(_this.options, json, function (parameters) {
                _this.write(parameters);
            });
        });
    }
};

module.exports = {
    run: function() {
        console.info("Please provide aws configuration".bold.yellow + "\n");
        configure.promptAll();
    },
    configure: function(callback) {
        configure.promptMissing(function() {
            configure.load();
            callback();
        });
    }
};
