var AWS = require('aws-sdk');
var configure = require('./configure');
var promptConfirm = require('../prompt/confirm');

module.exports = {
    run: function(args) {
        var stackName = args[0];

        if (!stackName) {
            console.error('Missing stackName'.red);
            return;
        }

        promptConfirm.prompt(function (confirm) {
            if (!confirm) {
                return;
            }

            configure.configure(function() {
                var cf = new AWS.CloudFormation({apiVersion: '2010-05-15'});

                var params = {
                    StackName: stackName
                };

                cf.deleteStack(params, function(err, data) {
                    if (err) {
                        console.error(err.message.red);
                        return;
                    }

                    console.info('Done'.green);
                });
            });
        });
    }
};
