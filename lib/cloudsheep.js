var configure = require('./actions/configure');
var validate = require('./actions/validate');
var events = require('./actions/events');
var createStack = require('./actions/create');
var updateStack = require('./actions/update');
var deleteStack = require('./actions/delete');
var describeStack = require('./actions/describe');
var listStacks = require('./actions/list');

module.exports = {
    configure: function(args) {
        configure.run();
    },
    validate: function(args) {
        validate.run(args);
    },
    events: function(args) {
        events.run(args);
    },
    create: function(args) {
        createStack.run(args);
    },
    update: function(args) {
        updateStack.run(args);
    },
    delete: function(args) {
        deleteStack.run(args);
    },
    describe: function(args) {
        describeStack.run(args);
    },
    list: function(args) {
        listStacks.run();
    }
};
