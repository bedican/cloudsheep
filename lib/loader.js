var fs = require('fs');
var config = require('./config');

module.exports= {
        config: function(filename, callback) {
            this.load(filename, function(data) {
                callback(config.new(data));
            });
        },
        load: function(filename, callback) {
            fs.readFile(filename, 'utf8', function (err, data) {
                if(err) throw err;
                callback(data);
            });
        }
};
