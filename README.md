# CloudSheep

A command-line tool for amazon cloud formation stack management

## Installation

``` bash
  $ [sudo] npm install -g cloudsheep
```

## Configuration
Aws configuration needs to be present within the file `.cloudsheep.aws.json` within the current working directory containing aws credentials.
Missing options will be asked for when running an action or options can be set by running the configure action.

```json
{
    "accessKeyId": "<access-key>",
    "secretAccessKey": "<secret-access-key>",
    "region": "eu-west-1"
}
```

## Usage
Using cloudsheep is relatively straight forward. The following actions can be performed.

### Configure
Configure aws options.

``` bash
  $ cloudsheep configure
```

### Validate
Validate a given cloud formation template.

``` bash
  $ cloudsheep validate <template.json>
```

### Create
Create a stack given a cloud formation template. You will be prompted for parameters defined within the template.

``` bash
  $ cloudsheep create <template.json>
```

### Update
Update a stack given a cloud formation template. You will be prompted for parameters defined within the template.

``` bash
  $ cloudsheep update <template.json>
```

### Delete
Delete a stack given a stack name.

``` bash
  $ cloudsheep delete <stack name>
```

### Describe
Describe a stack given a stack name.

``` bash
  $ cloudsheep describe <stack name>
```

### Events
View events for a stack given a stack name.

``` bash
  $ cloudsheep events <stack name>
```

### List
List current stacks. Currently only lists successfully created stacks.

``` bash
  $ cloudsheep list
```

#### License: MIT
#### Author: [Ash Brown](https://bitbucket.org/bedican)
